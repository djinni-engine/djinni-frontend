import Link from "next/link";
import Head from 'next/head';
import Router from 'next/router';
import nProgress from "nprogress";

Router.onRouteChangeStart = url => {
    console.log(url);
    nProgress.start();
}

Router.onRouteChangeComplete = () => nProgress.done()
Router.onRouteChangeError = () => nProgress.done()

export default ({ children, title }) => (
    <div className="root">
        <Head>
            <title>Djinni Engine</title>
        </Head>
        <header> 
            <Link href="/"><a>Home</a></Link>
            <Link href="/play"><a>Play</a></Link>
            <Link href="/about"><a>About</a></Link>
        </header>


            <h1>{title}</h1>

            {children}


        {/* <footer>made with magic! {new Date().toDateString()}</footer> */}
        <footer>((((made with magic)))</footer>
        <style jsx>{`
            .root {
                display: flex;
                justify-content: center;
                align-items: center;
                flex-direction: column;
            }
            header {
                width: 100%;
                display: flex;
                justify-content: space-around;
                padding: 1em;
                font-size: 1.2rem;
                background: teal;
            }
            header a {
                color: darkgrey;
                text-decoration: none;
            }
            footer {
                padding: 1em;
                font-size: 0.8rem;
            }
        `}</style>
        <style global jsx>{`
            body {
                margin: 0;
                font-size: 110%;
                background: #f0f0f0;
            }
        `} </style>


    </div>
);