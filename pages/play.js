import Link from 'next/link';
import Layout from '../components/Layout';
import { Component } from 'react';
import { CATEGORIES_URL } from "../utils/backend_urls";
import fetch from "isomorphic-unfetch";
import {CATEGORIES_URL_RESPONSE, CATEGORY_URL_RESPONSE,FILTER_HERO_DOG, FILTER_HERO_HUMAN, FILTER_HERO_HUMAN_KICK_FUNNY } from "../utils/mocked_responses";


export default class Play extends Component {
    static async getInitialProps () {

        // TODO I have to deploy my backend OR figure out how to send requests to localhost

        // const res = await fetch("https://api.github.com/users/djudjuu")
        // console.log(CATEGORIES_URL)
        const res = await fetch(CATEGORIES_URL);
        const statusCode = res.status > 200 ? res.status : false;
        const data = await res.json();
        return {"categories": data['data'] }
    }

    render () {
        // console.log('props', this.props)
        const { categories } = this.props;
        // console.log('cat', categories)
        const listItems = categories.map((c) =>
            <li>{c}</li>
        );
        return (
            <Layout title="Let's go">
                <p>Tell me what you're looking for! </p>
                <img src="/static/djinni.png" alt="djinni" height="200px" />
                <p>You can choose to filter for any of these categories: </p>
                <ul>{listItems}</ul>
            </Layout>
        );
    }
}

