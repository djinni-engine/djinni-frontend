import Link from 'next/link';
import Layout from '../components/Layout';

export default () => (
    <Layout title="About">
        <p>Magic being from the 42th dimension</p>
        <img src="/static/djinni.png" alt="djinni" height="200px" />
    </Layout>
);
