
const BACKEND_PORT = "3000"
const LOCAL_BACKEND_BASE_URL = "http://127.0.0.1:" + BACKEND_PORT + "/v1"
export const CATEGORIES_URL = LOCAL_BACKEND_BASE_URL + "/book/categories"
