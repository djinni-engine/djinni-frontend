export const CATEGORIES_URL_RESPONSE =  { "data": [ "language", "hero", "kick", "duration" ] }

export const CATEGORY_URL_RESPONSE = {
    hero: { "data": [ "human", "dog", "child" ] },
    language: { "data": [ "english", "german", "french" ] },
    kick: { "data": [ "character_development", "brainy", "funny", "real" ] },
    duration: { "data": [ "short", "medium", "longer", "epic" ] }
}

export const BOOKID_ID_FF3_RESPONSE = {
   "loginfo": "from db",
    "book": {
      "book_id": "ff37116eb80a11ea9d638bc720ec66fd",
      "title": "My new book",
      "kick": "funny",
      "hero": "human",
      "duration": "short",
      "language": "english"
    }
  }

export const UNKNOWN_BOOK_ID_RESPONSE = {
    status: 404, 
    data: { "detail": "Unknown book_id" }
}


export const FILTER_HERO_HUMAN = {
    "books": [
      "My new book",
      "too like the lightning",
      "Surely you re joking Mr, Feynman"
    ],
    "options": {
      "language": {
        "english": 3
      },
      "kick": {
        "brainy": 1,
        "funny": 2
      },
      "duration": {
        "epic": 1,
        "short": 2
      }
    }
  }

export const FILTER_HERO_HUMAN_KICK_FUNNY = {
    "books": [
      "My new book",
      "Surely you re joking Mr, Feynman"
    ],
    "options": {
      "language": {
        "english": 2
      },
      "duration": {
        "epic": 1,
        "short": 1
      }
    }
  }

export const FILTER_HERO_DOG = {
    "books": [
      "call of the wild"
    ],
    "options": {
        "kick": {
          "real": 1
        },
        "language": {
          "english": 1
        },
        "duration": {
          "short": 1
        }
    }
}